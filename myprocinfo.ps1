#1 - Hvem er jeg og hva er navnet p� dette scriptet?
#2 - Hvor lenge er det siden siste boot?
#3 - Hvor mange prosesser og tr�der finnes?
#4 - Hvor mange context switcer fant sted siste sekund?
#5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
#6 - Hvor mange interrupts fant sted siste sekund?
#9 - Avslutt dette scriptet
#Velg en funksjon:


# 1 - Hvem er jeg og hva er navnet p� dette scriptet?

function menu(){

write-host "
1 - Hvem er jeg og hva er navnet p� dette scriptet? `n
2 - Hvor lenge er det siden siste boot? `n
3 - Hvor mange prosesser og tr�der finnes? `n
4 - Hvor mange context switcer fant sted siste sekund? `n
5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund? `n
6 - Hvor mange interrupts fant sted siste sekund? `n
9 - Avslutt dette scriptet `n
Velg en funksjon: 
"

}
# 1 - Hvem er jeg og hva er navnet p� dette scriptet?
function whoami(){
    Write-Host "Navnet p� dette scriptet er: "$MyInvocation.ScriptName "Navnet p� brukeren er: $([System.Security.Principal.WindowsIdentity]::GetCurrent().Name) "
}

# 2 - Hvor lenge er det siden siste boot?
function uptime(){                         # Funksjonen er l�nt fra https://4sysops.com/archives/calculating-system-uptime-with-powershell/
   $os = Get-WmiObject win32_operatingsystem
   $uptime = (Get-Date) - ($os.ConvertToDateTime($os.lastbootuptime))
   $Display = "Uptime: " + $Uptime.Days + " days, " + $Uptime.Hours + " hours, " + $Uptime.Minutes + " minutes" 
   Write-Output $Display
}

#3 - Hvor mange prosesser og tr�der finnes?
function processes(){
$processes=0
$threads=0
foreach($i in $(get-process)) {
   $processes = $processes + 1
}
foreach($n in $(Get-WmiObject win32_thread)){
    $threads = $threads + 1
}
write-host "Det finnes $processes prosseser og $threads antall tr�der"

}

# 4 - Hvor mange context switcer fant sted siste sekund?
function context() {
Clear-Host
  $contextSwitcher = (Get-CimInstance -ClassName Win32_PerfFormattedData_PerfOS_System).ContextSwitchesPersec
  
  Write-Output "  Det skjedde $contextSwitcher context switcher det siste sekundet `n`n"

}
# 5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
function cpuTime() {
Clear-Host
  # M� tydligvis bruke norske navn siden OS-et er p� norsk... (Get-Counter -ListSet Prosessor | select -ExpandProperty Paths)
  $bruker = [math]::Round( ((Get-Counter "\prosessor(_total)\% Brukermodustid").CounterSamples).CookedValue , 2)
  $system = [math]::Round( ((Get-Counter "\prosessor(_total)\% Systemmodustid").CounterSamples).CookedValue , 2)
  
  Write-Output 
               "  Prosent av CPU-en brutk av user- og privileged mode det siste sekundet:"
               "    Usermode:        $bruker %" 
               "    Privileged mode: $system % `n`n"
 
}
# \Processor(*)\Interrupts/sec#
# 6 - Hvor mange interrupts fant sted siste sekund?
function interrupts() {

  Clear-Host
  $interupts = [math]::Round( ((Get-Counter "\Prosessor(_total)\Avbrudd/sek").CounterSamples).CookedValue , 0)

  Write-Output "  Det skjedde $interupts interupts det siste sekundet `n`n"

}


# Meny
do {
menu
$cmd = Read-Host
switch($cmd) {
    1 { whoami;break }
    2 { uptime; break }
    3 { processes; break }
    4 { context; break}
    5 { cpuTime; break}
    6 { interrupts; break}
    9 { break }
}
} while ($cmd -ne 9)